import Link from 'next/link'
import Head from 'next/head'

import {
  Button,
  Container,
  Icon
} from 'semantic-ui-react'

let nextConfig = require('../next.config.json')

class Navigation extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      // active: this.props.active
    }
  }

  render() {
    return (
      <header style={{ background: '#ccc', marginBottom: '1em', padding: '2em' }}>
        <Container text>
          <Button basic color='black' icon onClick={this.props.handleToggleSidebar}><Icon name='sidebar' /></Button>
          {
            nextConfig.navigation.map((nav) => {
              return (
                <Button className='mobile hidden' href={nav.href} basic={nav.title !== 'Portfolio'} color='black'>{nav.title}</Button>
              )
            })
          }
          <div style={{ float: 'right', textAlign: 'right', fontSize: '2em', lineHeight: '1em' }}>
            <Link href='https://trabur.metaheap.io'>
              <a title='Travis Burandt' style={{ color: '#111' }}><strong>TRABUR</strong></a>
            </Link>
          </div>
        </Container>
      </header>
    )
  }
}

export default Navigation