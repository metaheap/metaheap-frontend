import Link from 'next/link'
import Head from 'next/head'

import {
  Icon
} from 'semantic-ui-react'

class Logo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  
  render() {
    let fontSize = '8em'
    let mobileFontSize = '5em'
    return (
      <>
        <div className='mobile hidden' style={{ textAlign: 'center' }}>
          <Link href='https://trabur.metaheap.io'>
            <a title='TRABUR METAHEAP'>
              <Icon name='circle' style={{ color: '#1CA4FC', fontSize: fontSize, lineHeight: '110px' }} />
              <Icon name='play' style={{ color: '#33A64B', fontSize: fontSize, lineHeight: '110px' }} />
              <Icon name='square' style={{ color: '#F7BF4D', fontSize: fontSize, lineHeight: '110px'}} />
            </a>
          </Link>
        </div>
        <div className='mobile only' style={{ textAlign: 'center' }}>
          <Link href='https://trabur.metaheap.io'>
            <a title='TRABUR METAHEAP'>
              <Icon name='circle' style={{ color: '#1CA4FC', fontSize: mobileFontSize, lineHeight: '60px' }} />
              <Icon name='play' style={{ color: '#33A64B', fontSize: mobileFontSize, lineHeight: '60px' }} />
              <Icon name='square' style={{ color: '#F7BF4D', fontSize: mobileFontSize, lineHeight: '60px'}} />
            </a>
          </Link>
        </div>
      </>
    )
  }
}

export default Logo