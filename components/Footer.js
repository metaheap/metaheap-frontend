import Link from 'next/link'
import Head from 'next/head'

import {
  Button,
  Container,
  Grid,
  Icon,
  Card,
  Image
} from 'semantic-ui-react'

let project = require('../package.json')
let nextConfig = require('../next.config.json')

class Footer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      // active: this.props.active
    }
  }

  render() {
    return (
      <footer>
        <div style={{ background: '#fff' }}>
          <Container text style={{ textAlign: 'center', padding: '4em 0 4em 0' }}>
            <h1 style={{ margin: 0 }}>Free Software and Services</h1>
            <p>Here are a couple free projects that I have in the works.</p>
            <Grid stackable>
              {
                nextConfig.eCommerce
                  ?
                    <Grid.Column width={8}>
                      <Card fluid href={`//${nextConfig.eCommerce}`}>
                        <Card.Content>
                          <Card.Header><Icon name='cart' /> eCommerce</Card.Header>
                          <Card.Description style={{ textAlign: 'left' }}>
                            Everything available for sale in the store is fully customizable. With this code you can sell software (like shop.metaheap.io) or even promotional products (like printedbasics.com). Contact us and we will help you setup, run, and host a store.  
                          </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                          <Button fluid size='massive' as='a' href={`//${nextConfig.eCommerce}`} basic color='black'>{nextConfig.eCommerce}</Button>
                        </Card.Content>
                      </Card>
                    </Grid.Column>
                  : null
              }
              {
                nextConfig.printer
                  ?
                    <Grid.Column width={8}>
                      <Card fluid href={`//${nextConfig.printer}`}>
                        <Card.Content>
                          <Card.Header><Icon name='print' /> Printer</Card.Header>
                          <Card.Description style={{ textAlign: 'left' }}>
                            We use this behind the scenes to print custom product after custom product. Basically this printer merges multiple repositorys together (a template repo and content repo), then runs commands (like install, build, and export), and lastly manages production infrastructure (such as pushing the generated static website to an amazon s3 bucket).
                          </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                          <Button fluid size='massive' as='a' href={`//${nextConfig.printer}`} basic color='black'>{nextConfig.printer}</Button>
                        </Card.Content>
                      </Card>
                    </Grid.Column>
                  : null
              }
            </Grid>
          </Container>
        </div>
        <div style={{ background: '#ccc' }}>
          <Container text style={{ textAlign: 'center', padding: '4em 0 4em 0' }}>
            <h1 style={{ margin: 0 }}>Paid Software and Services</h1>
            <p>My current goto software and services that I'm working on. For the HVAC industry, hvac-buddy.com, runs on both Apple and Android mobile device platforms. While for the field service industry, fleetgrid.com, runs on both mobile device platforms and as a cloud based web app.</p>
            <Grid columns={2} stackable>
              <Grid.Row>
                <Grid.Column>
                  <Button fluid size='massive' target='_blank' as='a' href='https://www.hvac-buddy.com' basic color='black'>hvac-buddy.com</Button>
                </Grid.Column>
                <Grid.Column>
                  <Button fluid size='massive' target='_blank' as='a' href='https://www.fleetgrid.com' basic color='black'>fleetgrid.com</Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </div>
        <div style={{ background: '#333', padding: '3em 0' }}>
          <Container>
            <Grid stackable>
              <Grid.Row>
                <Grid.Column width={6} className='mobile hidden' style={{ textAlign: 'right', marginTop: '1.65em' }}>
                  <nav>
                    <Link href={`https://${nextConfig.eCommerce}`}><a style={{ fontSize: '1.2em', margin: '1em' }}>SHOP</a></Link>
                    <Link href={`https://${nextConfig.printer}`}><a style={{ fontSize: '1.2em', margin: '1em' }}>PRINTER</a></Link>
                  </nav>
                </Grid.Column>
                <Grid.Column width={6} className='mobile only' style={{ textAlign: 'center', marginTop: '1.65em' }}>
                  <nav>
                    <Link href={`https://${nextConfig.eCommerce}`}><a style={{ fontSize: '1.2em', margin: '1em' }}>SHOP</a></Link>
                    <Link href={`https://${nextConfig.printer}`}><a style={{ fontSize: '1.2em', margin: '1em' }}>PRINTER</a></Link>
                  </nav>
                </Grid.Column>
                <Grid.Column width={4} className='mobile hidden tablet hidden computer hidden' style={{ textAlign: 'center' }}>
                  <Link href="/" >
                    <a title={nextConfig.textLogo}>
                      <Button size='massive' color='grey' icon inverted basic style={{ marginRight: '0' }}>
                        <div>
                          {nextConfig.textLogo}
                        </div>
                      </Button>
                    </a>
                  </Link>
                </Grid.Column>
                <Grid.Column width={4} className='computer only' style={{ textAlign: 'center' }}>
                  <Link href="/" >
                    <a title={nextConfig.textLogo}>
                      <Button size='massive' color='grey' icon inverted basic style={{ marginRight: '0' }}>
                        <div>
                          {nextConfig.textLogo}
                        </div>
                      </Button>
                    </a>
                  </Link>
                </Grid.Column>
                <Grid.Column width={4} className='tablet only' style={{ textAlign: 'center' }}>
                  <Link href="/" >
                    <a title={nextConfig.textLogo}>
                      <Button size='massive' color='grey' icon inverted basic style={{ marginRight: '0' }}>
                        <div>
                          {nextConfig.textLogoShort}
                        </div>
                      </Button>
                    </a>
                  </Link>
                </Grid.Column>
                <Grid.Column width={4} className='mobile only' style={{ textAlign: 'center' }}>
                  <Link href="/" >
                    <a title={nextConfig.textLogo}>
                      <Button size='massive' color='grey' icon inverted basic style={{ marginRight: '0' }}>
                        <div>
                          {nextConfig.textLogo}
                        </div>
                      </Button>
                    </a>
                  </Link>
                </Grid.Column>
                <Grid.Column width={6} className='mobile hidden' style={{ textAlign: 'left', marginTop: '1.65em' }}>
                  <nav>
                    <Link href='https://trabur.metaheap.io/about'><a style={{ fontSize: '1.2em', margin: '1em' }}>ABOUT</a></Link>
                    <Link href='https://trabur.metaheap.io/contact'><a style={{ fontSize: '1.2em', margin: '1em' }}>CONTACT</a></Link>
                  </nav>
                </Grid.Column>
                <Grid.Column width={6} className='mobile only' style={{ textAlign: 'center', marginTop: '1.65em' }}>
                  <nav>
                    <Link href='https://trabur.metaheap.io/about'><a style={{ fontSize: '1.2em', margin: '1em' }}>ABOUT</a></Link>
                    <Link href='https://trabur.metaheap.io/contact'><a style={{ fontSize: '1.2em', margin: '1em' }}>CONTACT</a></Link>
                  </nav>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </div>
        <div className='mobile hidden' style={{ background: '#000', padding: '2em 0', color: '#aaa' }}>
          <Container text>
            <nav style={{ float: 'right' }}>
              Powered by <Link href='https://trabur.metaheap.io'><a title='Travis Burandt' target='_blank'>TRABUR</a></Link> v{project.version}
            </nav>
            This site is copyright © TRABUR {(new Date()).getFullYear()}.<br />
            Design and logo copyright © TRABUR {(new Date()).getFullYear()}.
          </Container>
        </div>
        <div className='mobile only' style={{ background: '#000', padding: '2em 0', color: '#aaa' }}>
          <Container text>
            <nav style={{ textAlign: 'center' }}>
              This site is copyright © TRABUR {(new Date()).getFullYear()}.<br />
              Design and logo copyright © TRABUR {(new Date()).getFullYear()}.<br />
              Powered by <Link href='https://trabur.metaheap.io'><a title='Travis Burandt' target='_blank'>TRABUR</a></Link> v{project.version}
            </nav>
          </Container>
        </div>
      </footer>
    )
  }
}

export default Footer