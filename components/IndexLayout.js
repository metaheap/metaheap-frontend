import Link from 'next/link'
import Head from 'next/head'

import { initGA, logPageView } from '../utils/analytics'

import Navigation from './Navigation'

import Particles from 'react-particles-js'
import Logo from './Logo'
import Footer from './Footer'
import SidebarNavigation from './Sidebar'

import {
  Menu,
  Sidebar
} from 'semantic-ui-react'

class IndexLayout extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      children: this.props.children,
      title: this.props.title || 'This is the default title', 
      meta: this.props.meta,
      visible: false
    }
  }

  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()
  }

  handleToggleSidebar = () => {
    this.setState({ 
      visible: !this.state.visible
    })
  }

  render() {
    const { visible } = this.state

    return (
      <div>
        <Head>
          <title>{this.state.title}</title>
          <meta name='description' content={this.state.meta} />
          <meta charSet='utf-8' />
          <meta name='viewport' content='width=500, initial-scale=1.0, width=device-width' />
          <link rel='shortcut icon' type='image/x-icon' href='/static/favicon.ico' />
          <link rel="apple-touch-icon" sizes="57x57" href="/static/apple-icon-57x57.png" />
          <link rel="apple-touch-icon" sizes="60x60" href="/static/apple-icon-60x60.png" />
          <link rel="apple-touch-icon" sizes="72x72" href="/static/apple-icon-72x72.png" />
          <link rel="apple-touch-icon" sizes="76x76" href="/static/apple-icon-76x76.png" />
          <link rel="apple-touch-icon" sizes="114x114" href="/static/apple-icon-114x114.png" />
          <link rel="apple-touch-icon" sizes="120x120" href="/static/apple-icon-120x120.png" />
          <link rel="apple-touch-icon" sizes="144x144" href="/static/apple-icon-144x144.png" />
          <link rel="apple-touch-icon" sizes="152x152" href="/static/apple-icon-152x152.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-icon-180x180.png" />
          <link rel="icon" type="image/png" sizes="192x192"  href="/static/android-icon-192x192.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="96x96" href="/static/favicon-96x96.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png" />
          <link rel="manifest" href="/static/manifest.json" />
          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta name="msapplication-TileImage" content="/static/ms-icon-144x144.png" />
          <meta name="theme-color" content="#ffffff" />
          <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css' />
          <link rel='stylesheet' href='/static/index.css' />
        </Head>
        <Particles
          width="100%"
          height="100%"
          style={{ 
            position: 'absolute',
            top: '0',
            right: '0',
            bottom: '0',
            left: '0',
            background: '#111',
            zIndex: 0
          }}
        />
        <div style={{
            position: 'absolute',
            top: '0',
            right: '0',
            bottom: '0',
            left: '0',
            zIndex: 1,
            overflowY: 'auto'
        }}>
          <Sidebar.Pushable>
            <Sidebar
              as={Menu}
              animation='push'
              icon='labeled'
              inverted
              onHide={this.handleToggleSidebar}
              vertical
              visible={visible}
              width='thin'
            >
              <SidebarNavigation />
            </Sidebar>
            <Sidebar.Pusher dimmed={visible}>
              <Navigation handleToggleSidebar={this.handleToggleSidebar} />
              <Logo />
              <main style={{ minHeight: '500px' }}>
                {this.state.children}
              </main>
              <Footer />
            </Sidebar.Pusher>
          </Sidebar.Pushable>
        </div>
      </div>
    )
  }
}

export default IndexLayout