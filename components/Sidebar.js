import Link from 'next/link'
import Head from 'next/head'

import {
  Button,
  Menu,
  Icon
} from 'semantic-ui-react'

import nextConfig from '../next.config.json'

class Sidebar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      active: this.props.active
    }
  }

  render() {
    return (
      <>
        {
          nextConfig.sidebar.map((nav, index) => {
            return (
              <Menu.Item key={index} as='a' href={nav.href}>
                <Icon name={nav.icon} />
                {nav.title}
              </Menu.Item>
            )
          })
        }
      </>
    )
  }
}

export default Sidebar