import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import IndexLayout from '../components/IndexLayout'

import {
  Container
} from 'semantic-ui-react'

class Index extends React.Component {
  static async getInitialProps (context, apolloClient) {
    
    return {}
  }

  constructor(props) {
    super(props);
    
    this.state = {
    }
  }

  render () {
    return (
      <IndexLayout 
        title='My Current Tools - TRABUR'
        meta={`What are some of the tools that I use as a developer? ~Travis Burandt`}
      >
        <Container 
          text 
          style={{
            background: 'rgba(255,255,255, 0.3)',
            border: '2px solid #fff',
            padding: '1em',
            color: '#fff',
            fontSize: '1.3em',
            marginTop: '1em',
            marginBottom: '1em'
          }}
        >
          <div>So what is <Link href='/my-current-tools'><a>my current tools</a></Link>et as a developer? I am not only just going to talk about tools on a toolbelt (like git and slack) but also the utilities or monthly subscription based tools (like google fiber and google cloud platform).</div>
          <br />  
          <div>Things aren't stuck in stone and my tools are constantly changing but what is the driving factor behind all of this? I enjoyed math class when what we learned from the previous day built up and needed to be applied in order to learn new things the next day. Such as needing to learn addition as a base in order to learn multiplication. I don't actually enjoy math but rather the process involved in achieving some goal.</div>
          <br />
          <div>When I was a bike racer my muscles were geared more towards time trials (TT), or my favorite: forming a lead out train to get our team sprinter on the podium. If you were to ask me what my current toolset was I would point to my bike and/or the road. Things are quite different as a coder.</div>
          <br />
          <div>I started out with PHP as my first programming language because the software that I was using to run my site (<Link href='//halotracks.org'><a>halotracks.org</a></Link>) was programmed in that. I had taken some HTML and CSS classes in high school but I don't think these count as real programming languages.</div>
          <br />
          <div>If I were to start over and choose something other than PHP or tell someone new to coding what programming language to learn first I would say choose the piece of software that interests you the most then figure out what programming language it is writen in and learn that first.</div>
          <br />
          <div>I continued to code in PHP and more specifically around the <Link href='https://www.yiiframework.com/'><a target='_blank'>yiiframework.com</a></Link> because I was interested in building database backed dynamic web apps where I didn't need to know JavaScript. Looking back I wish I was more involved with <Link href='https://laravel.com/'><a target='_blank'>laravel.com</a></Link> but that wasn't a thing back then. I was soon employed at my first job <Link href='https://www.crunchbase.com/organization/gritness'><a target='_blank'>gritness.com</a></Link> to code Yii.</div> 
          <br />
          <div>But it wasn't long before I looked over the fence onto the other side where everything appeared to be all green grass which was JavaScript, <Link href='https://angular.io/'><a target='_blank'>angular.io</a></Link>, and node.js. Turns out my next couple of jobs were frontend developer programming in these technologies. React and react-native soon became a thing and prospects of only needed to know JavaScript, HTML, and CSS in order to develop full stack (mobile (react-native), backend (node.js), frontend (next.js), cloud (pulumi), etc) was now a possibility.</div>
          <br />
          <div>That and with the introduction of containers and serverless the question becomes should a website/web app run inside:</div>
          <ol>
            <li>
              container (kubernetes)
            </li>
            <li>
              serverless (aws lambda, gcf)
            </li>
            <li>
              static (netlify, s3, IPFS)
            </li>
          </ol>
          <div>Each has its positives and negatives and it usually depends on what your building but typically as a developer I try to lean towards option 3 (static) first, then option 2 (serverless) second, and finally option 1 (containers) last.</div>
          <br />
          <div>/my-current-tools is still in the works!</div>
          <br />
          <div>~Travis Burandt</div>
        </Container>
      </IndexLayout>
    )
  }
}

export default Index