import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import IndexLayout from '../components/IndexLayout'

import {
  Container
} from 'semantic-ui-react'

import nextConfig from '../next.config.json'

class Index extends React.Component {
  static async getInitialProps (context, apolloClient) {
    
    return {}
  }

  constructor(props) {
    super(props);
    
    this.state = {
    }
  }

  render () {
    return (
      <IndexLayout 
        title='Travis Burandt - TRABUR'
        meta={`Welcome to trabur.metaheap.io, my simple portfolio to showcase some of the projects that I'm working on. ~Travis Burandt`}
      >
        <Container 
          text 
          style={{
            background: 'rgba(255,255,255, 0.3)',
            border: '2px solid #fff',
            padding: '1em',
            color: '#fff',
            fontSize: '1.3em',
            marginTop: '1em',
            marginBottom: '2em'
          }}
        >
          <div>Welcome to <Link href='/'><a>trabur.metaheap.io</a></Link>, my simple portfolio to showcase some of the projects that I'm working on.</div>
          <ul>
            <li>I have history in the <Link href='http://halotracks.org'><a target='_blank'>gaming industry</a></Link>, <Link href='https://www.crunchbase.com/organization/gritness'><a target='_blank'>fitness industry</a></Link>, <Link href='https://www.docbookmd.com/'><a target='_blank'>healthcare industry</a></Link>, <Link href='https://www.hvac-buddy.com'><a target='_blank'>HVAC industry</a></Link>, <Link href='https://www.fleetgrid.com'><a target='_blank'>field service industry</a></Link>, and <Link href='https://www.pnimedia.com'><a target='_blank'>print</a></Link> <Link href='https://www.printedbasics.com'><a target='_blank'>industry</a></Link>. (10+ years of experience coding production grade user interfaces)</li>
            <li>As a software developer, I am mostly interested in using <Link href='/my-current-tools'><a>my current tools</a></Link> in ways that benifit everyone and myself. Take a look at some of the <Link href={`https://${nextConfig.eCommerce}`}><a>fully customizable software</a></Link> that I have for sale in my shop.</li>
            <li>If you think you have a good idea for me to develop, a new tool for me to use, a better line of code for me to write, please send it to me :).</li>
          </ul>
          <div>Checkout my current projects using the links below. Feel free to read more <Link href='/about'><a>about me</a></Link> and <Link href='/contact'><a>contact me</a></Link> for any feedback or queries that you may have.</div>
          <br />
          <div>~Travis Burandt</div>
        </Container>
      </IndexLayout>
    )
  }
}

export default Index