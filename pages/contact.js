import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import IndexLayout from '../components/IndexLayout'

import {
  Container
} from 'semantic-ui-react'

class Index extends React.Component {
  static async getInitialProps (context, apolloClient) {
    
    return {}
  }

  constructor(props) {
    super(props);
    
    this.state = {
    }
  }

  render () {
    return (
      <IndexLayout 
        title='Contact Travis - TRABUR'
        meta={`Here is how you can contact me. ~Travis Burandt`}
      >
        <Container 
          text 
          style={{
            background: 'rgba(255,255,255, 0.3)',
            border: '2px solid #fff',
            padding: '1em',
            color: '#fff',
            fontSize: '1.3em',
            marginTop: '1em',
            marginBottom: '1em'
          }}
        >
          <div>Send an email to travis.burandt@gmail.com.</div>
          <br />
          <div>~Travis Burandt</div>
        </Container>
      </IndexLayout>
    )
  }
}

export default Index